import QtQuick 2.11
import QtQuick.Controls 2.4

Page {
    width: 280
    height: 300
    property alias butonAdunare: roundButton
    property alias butonScadere: roundButton1
    property alias butonImpartire: roundButton2
    property alias butonInmultire: roundButton3

    property alias numar1: numar1
    property alias numar2: numar2
    property alias rezultat_txt: textField2.text




    header: Label {
        text: qsTr("Calculator simplu")
        font.pixelSize: Qt.application.font.pixelSize * 2
        padding: 10
    }

    RoundButton {
        id: roundButton
        x: 20
        y: 115
        text: "+"
    }

    RoundButton {
        id: roundButton1
        x: 85
        y: 115
        text: "-"
    }

    RoundButton {
        id: roundButton2
        x: 156
        y: 115
        text: "/"
    }

    RoundButton {
        id: roundButton3
        x: 226
        y: 115
        text: "*"
    }

    TextField {
        id: numar1
        x: 117
        y: 18
        width: 126
        height: 40
        text: qsTr("Text Field")
        font.family: "Tahoma"
    }

    TextField {
        id: numar2
        x: 117
        y: 69
        width: 126
        height: 40
        text: qsTr("Text Field")
        font.family: "Tahoma"
    }

    TextField {
        id: textField2
        x: 117
        y: 189
        width: 126
        height: 40
        text: qsTr("Text Field")
        horizontalAlignment: Text.AlignHCenter
        font.wordSpacing: 0.2
        font.family: "Tahoma"
        readOnly: true
    }

    Label {
        id: label
        x: 43
        y: 25
        width: 62
        height: 26
        text: qsTr("Numar 1")
        font.pointSize: 13
        horizontalAlignment: Text.AlignHCenter
    }

    Label {
        id: label1
        x: 43
        y: 76
        width: 62
        height: 26
        text: qsTr("Numar 2")
        horizontalAlignment: Text.AlignHCenter
        font.pointSize: 13
    }

    Label {
        id: label2
        x: 43
        y: 196
        width: 62
        height: 26
        text: qsTr("Rezultat")
        horizontalAlignment: Text.AlignHCenter
        font.pointSize: 13
    }
}
