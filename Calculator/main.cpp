#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include<calculatorsimplu.h>
int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;
    QObject* mroot = engine.rootObjects().first()->findChild<QObject*>("CalculatorSimplu");
    CalculatorSimplu c(nullptr,mroot);

    return app.exec();
}
