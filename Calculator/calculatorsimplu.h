#ifndef CALCULATORSIMPLU_H
#define CALCULATORSIMPLU_H
#include<string.h>
#include <QObject>

class CalculatorSimplu : public QObject
{
    Q_OBJECT
public:
    explicit CalculatorSimplu(QObject *parent = nullptr, QObject* mroot=nullptr);

private:
    QObject *mRoot;
    float rezultat;
    void actualizeazaRezultat();

signals:

public slots:
    void executaOperatie(QString a, QString b,QString operatie);
    void doIt();

};

#endif // CALCULATORSIMPLU_H
