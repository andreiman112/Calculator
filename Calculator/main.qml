import QtQuick 2.11
import QtQuick.Controls 2.4

ApplicationWindow {
    visible: true
    width: 280
    height: 330
    title: qsTr("Calc")

    SwipeView {
        id: swipeView
        anchors.fill: parent
        currentIndex: tabBar.currentIndex

        Page1Form {
            id: page_c_simplu
            objectName: "CalculatorSimplu"
            signal faOperatie(string a, string b, string operatie)
            signal doIt()
//            property  alias rezultat: rezultat_txt

            butonAdunare.onClicked: {
                page_c_simplu.faOperatie(numar1.text, numar2.text,"+")
}

            butonScadere.onClicked: {
                page_c_simplu.faOperatie(numar1.text, numar2.text,"-")
}

            butonImpartire.onClicked: {
                page_c_simplu.faOperatie(numar1.text, numar2.text,"/")
}
            butonInmultire.onClicked: {
             page_c_simplu.faOperatie(numar1.text, numar2.text,"*")
            }

        }

        Page2Form {
        }
    }

    footer: TabBar {
        id: tabBar
        currentIndex: swipeView.currentIndex

        TabButton {
            text: qsTr("Calculator simplu")
        }
        TabButton {
            text: qsTr("Calculator stiintific")
        }
    }
}
