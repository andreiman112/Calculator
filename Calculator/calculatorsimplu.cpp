#include "calculatorsimplu.h"
#include<string.h>
#include<QDebug>
CalculatorSimplu::CalculatorSimplu(QObject *parent, QObject* mroot) : QObject(parent)
{
    this->mRoot=mroot;

    QObject::connect(mroot,SIGNAL(faOperatie(QString,QString,QString)),this,SLOT(executaOperatie(QString,QString,QString)));


}


void CalculatorSimplu::executaOperatie(QString a, QString b, QString operatie)
{
        QLocale locale = QLocale();
        float a_=locale.toFloat(a);
        float b_=locale.toFloat(b);
        qDebug()<<a_<<endl;
          switch(operatie.toLatin1()[0])
        {
             case '+':rezultat=a_+b_; break;
             case '-':rezultat=a_-b_;break;
             case '/':rezultat=a_/b_;break;
             case '*':rezultat=a_*b_;break;
          }

          actualizeazaRezultat();
}
void CalculatorSimplu::actualizeazaRezultat()
{
    mRoot->setProperty("rezultat_txt",rezultat);
}

void CalculatorSimplu::doIt()
{

}
